
Pragmatic debian packaging for Lightbird for Thunderbird
========================================================


See also
--------

https://addons.mozilla.org/thunderbird/addon/lightbird/


Building
--------

Vanilla debian build:

    $ dpkg-buildpackage


Obtaining a new upstream version
--------------------------------

    $ git remote add upstream https://github.com/Exalm/lightbird/
    $ git fetch upstream
    $ git checkout upstream
    $ git merge upstream/master
